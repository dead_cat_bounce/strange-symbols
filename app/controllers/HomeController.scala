package controllers

import javax.inject._
import akka.util.ByteString
import play.api._
import play.api.mvc._
import play.api.http.HttpEntity

/**
 * This controller creates an `Action` to handle HTTP requests to the
 * application's home page.
 */
@Singleton
class HomeController @Inject() extends Controller {

  def home = Action {
    Ok(views.html.home("Hello fucko!"))
  }
  /**
   * Create an Action to render an HTML page with a welcome message.
   * The configuration in the `routes` file means that this method
   * will be called when the application receives a `GET` request with
   * a path of `/`.
   */
  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

  def myAction = Action { request =>
    val htmlResult2 = Ok(<h1>Hello World!</h1>).as(HTML)
//    Ok("Hello world")
    htmlResult2
  }

  def hello(name: String) = Action {
    Ok("Hello " + name)
  }

  def resultExample = Action {
    Result(
      header = ResponseHeader(200, Map.empty),
      body = HttpEntity.Strict(ByteString("Hello world!"), Some("text/plain"))
    )
  }

  def todo(name:String) = TODO

  def showId(id:Long) = Action {
    Ok(s"You have id $id")
  }

  def save = Action(parse.json) { request =>
    Ok("Got: " + (request.body \ "name").as[String])
  }

}
