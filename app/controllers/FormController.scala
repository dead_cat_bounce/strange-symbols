package controllers

import javax.inject._
import models.UserData
import play.api._
import play.api.mvc._
import play.api.http.HttpEntity
import play.api.data._
import play.api.data.Forms._
import play.api.i18n.{MessagesApi, I18nSupport}

/**
  * Created by bkarels on 4/22/16.
  */
@Singleton
class FormController @Inject()(val messagesApi: MessagesApi) extends Controller with I18nSupport {

  val userForm: Form[UserData] = Form(
    mapping(
      "name" -> nonEmptyText(minLength = 1, maxLength = 60),
      "age" -> number(min = 0, max = 100)
    )(UserData.apply)(UserData.unapply)
  )

  def createUser = Action {
    Ok(views.html.createUser(userForm))
  }

  def saveUser = Action { implicit request =>
    val userData: UserData = userForm.bindFromRequest.get
    val name: String = userData.name
    val age: Long = userData.age
    Ok(s"saved a user with name $name and of age $age")
  }
}
